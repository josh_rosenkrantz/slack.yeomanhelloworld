'use strict';
const Generator = require('yeoman-generator');
module.exports = class extends Generator {
  constructor(args, opts) {
    super(args, opts);
    this.log('Initializing...');
  }
  async prompting() {
    this.answers = await this.prompt([{
      type    : 'input',
      name    : 'message',
      message : 'Whats your name?',
    }]);
  }

  writing() {
    this.fs.copyTpl(
      this.templatePath('index.html'),
      this.destinationPath('public/index.html'),
      { message:'Hello ' +this.answers.message} // user answer `title` used
    );
  }
  //confirm()
  //{
     // this.log("File successfully saved.")
 // }
 
};